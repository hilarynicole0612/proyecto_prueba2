
const express = require('express');
const router = express.Router();
const Product = require('../model/product');
router.get('/edit/:id', async (req, res, next) => {
    const product = await Product.findById(req.params.id);
    console.log(product)
    res.render('edit', { product });
  });
  
  router.post('/edit/:id', async (req, res, next) => {
    const { id } = req.params;
    await Product.update({_id: id}, req.body);
    res.redirect('/');
  });
